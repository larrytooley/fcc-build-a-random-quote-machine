'use strict'

var quotes = [
    {
        quote: "I'm doing a (free) operating system (just a hobby, won't be big and professional like gnu) for 386(486) AT clones.",
        source: "Linus Torvalds",
        sfw: true
    },
    {
        quote: "If you still don't like it, that's OK: that's why I'm boss. I simply know better than you do.",
        source: "Linus Torvalds",
        sfw: true
    },
    {
        quote: "If Microsoft ever does applications for Linux it means I've won.",
        source: "Linus Torvalds",
        sfw: true
    },
    {
        quote: "Software is like sex: it's better when it's free.",
        source: "Linus Torvalds",
        sfw: true
    },
    {
        quote: "Talk is cheap. Show me the code.",
        source: "Linus Torvalds",
        sfw: true
    },
    {
        quote: "Microsoft isn't evil, they just make really crappy operating systems.",
        source: "Linus Torvalds",
        sfw: true
    }
]

var getQuote = function() {
    return quotes[Math.floor(Math.random()*quotes.length)].quote
}

var updateTweet = function(quote) {
    document.getElementById('tweet').href = 'https://twitter.com/intent/tweet?text=' + quote + ' -- Linus Torvalds'
}
var displayQuote = function() {
    var quote = getQuote()
    document.getElementById('quote').innerHTML = quote
    updateTweet(quote) 
}

$(document).ready(function(){
    displayQuote()
})

$('#shuffle').click(function(){
    displayQuote()
})

// Copyright Date Script
var today = new Date();
var year = today.getFullYear();
var copyright = '&copy; ' + year + ' Larry Tooley';

document.getElementById('copyright').innerHTML = copyright;